#Changelog
* build number will now always be set regardless of release type
* changed to getting previous release version info primarily from Git (**may tie the implementation to running in GitBash or *nix 'bash'**)
* backed out the ignoreBranches parameter.. Patches will now only be explicitly set and only set if releaseType=patch
======
* added command line argument ignoreBranches to disable checks for Git branch that causes release type of PATCH (for use in manual builds)
* added property to the version.* for release-plugin-version to show what version of the Release plugin was used
* added command line argument disableGit to disable the git Tagging/Checkin
* now keeping Build and Release version info in version files separately (Release versions only update after a release or patch)
* merged Git Plugin into Release Plugin
* will prevent 'flash-back' builds (attempt to create a greater version number for an older code baseline)
* version.properties > renamed to version.props
* Repeat Builds will no longer Bump Version (check of prev/curr git hash)
* PATCH builds now implemented if branch is non-'master'
* Build Number only Set for PATCH and SNAPSHOT
* Changed task names to getCurrVersionInfo (informational), getNextVersionInfo ('destructive')
* Factored out common ReleaseTypeUtil into a common source set
* Build Number Provided (command line) & Auto-Incremented now implemented 

# For Users of Plugin
## Required Dependencies (runtime)

* **git** - must be installed


## To Use in your Build Script (Consumer)
---
**1 - Provide your project name and submodule/description**
```groovy
ext {
  	artifactId = 'my-sso-jar' // what you want your base jar or war file name to be
	projectName = 'My Project Name'
  	archiveType = 'jar' // or war
}
description = 'Description of my project'
```

**2 - Place the following at the end of your build script *(prior to your main target tasks like 'jar' or 'war')* **
```groovy
buildscript {
	repositories {
		maven	{ url "http://artifact-server:8080/artifactory/libs-cots" 	}
		jcenter()
	}
	dependencies {
		classpath group: 'bracken.gradle',	name: 'release-plugin',	version: '16.3'
	}
}
apply plugin: 'release-plugin'

jar.doFirst {
	//Optionally place here or run during command line
	tasks.getNextVersionInfo.execute();
}

// War/Jar file configuration (the following is required, but there may be other information you require):
jar {

	//Includes the version.properties and version.json files created by 'bumpVersion' task
	from (project.projectDir) {
		include 'version.*'
	}

	//... rest of the war configuration
}
```

**Note: do not set the pom.version in your uploadArchives - this will be handled by the release plugin **

## Runtime Usage
---
### Automated Builds (CI/CD)
```groovy
// Build Number: If Environment property set (currently defaults to: 'GO_PIPELINE_COUNTER') - build number will follow the value of this property
// -- provided build number can be overridden by setting the tasks property 'buildNumProvided' to false

// build>
$gradle [ getCurrVersionInfo | getNextVersionInfo]	jar 			-DreleaseType=r

// build / local maven install>
$gradle [ getCurrVersionInfo | getNextVersionInfo]	install

//build and release>
$gradle [ getCurrVersionInfo | getNextVersionInfo]	bintrayUpload 	-Dapikey=[apikey]
or..
$gradle uploadArchives -DartifactoryPwd=[artifactoryPwd]
```

### Manual (Developer-initiated) Builds
```groovy
// print release type info>
$gradle printReleaseTypes

// build>
$gradle jar -DreleaseType=r

// build / local maven install>
$gradle install

// build and release>
$gradle bintrayUpload -Dapikey=[apikey]
// or..
$gradle uploadArchives -DartifactoryPwd=[artifactoryPwd]
```
#### Other Options
```groovy
// ignore check for re-releasing same code baseline>		
$gradle [ getCurrVersionInfo | getNextVersionInfo]	jar 			-DreleaseType=r	-DignoreGit

// disable Git checkin of version.* properties and Git tagging
$gradle [ getCurrVersionInfo | getNextVersionInfo]	jar 			-DreleaseType=r	-DdisableGit
```

### Effect of release type on version label:
**Note: BUILD NUMBER is not really significant other than being a indication of uniqueness**
* **BUILD NUMBER will only changed for SNAPSHOT and PATCH releases **(will help eliminate extraneous Git tags, etc)
**e.g. starting with a fresh project in the year 2016 (baseline release version label would = 16.0.1**

|    ReleaseType 	| 	ARCHIVE-POSTFIX        |        REPOSITORY          |  GIT ACTIONS
|:-----------------:|:------------------------:|:--------------------------:|:----------------------------------------------------------:|
|    SNAPSHOT   	|   **16.0.1-SNAPSHOT**    |      CI/CD will not save   |   no actions
|    REL CANDIDATE  |   **16.0.1-RELCAND**     |      libs-gots-snapshot    |   tag created 
|    RELEASE   		|	**16.1.1**             |      libs-gots-release	    |   version files checked in, tag created in 'master' branch
|    PATCH   		|   **16.1.1-PATCH**       |      libs-gots-release	    |   version files checked in, tag created in 'release' branch


## Sample Output:
### version.json
```json
{
    "build-type": "RELEASE",
    "release-type": "RELEASE",
    "module": "Gradleware Test Client using..ARPECS Data API - test for gradleware git/release project",
    "build-version": "16.1.77",
    "build-msg": "updated with al the test cases",
    "build-year": "16",
    "release-year": "16",
    "build-hash-short": "63d20fa",
    "release-version": "16.1.77",
    "release-msg": "updated with al the test cases",
    "release-hash-short": "63d20fa",
    "build-release": "1",
    "build-date": "Mon Apr 18 15:28:34 EDT 2016",
    "release-branch": "master",
    "release-date": "Mon Apr 18 15:28:34 EDT 2016",
    "release-number": "1",
    "build-number": "77",
    "build-branch": "master",
    "build-hash": "63d20fab1ba098bfe5a81bce8039099dc2693180",
    "release-hash": "63d20fab1ba098bfe5a81bce8039099dc2693180",
    "submodule": "Gradleware Test Client using..Authoritative Roles, Profile & Entitlements Control System (ARPECS)"
}
```

### version.prop
```
#Version Properties
#Mon Apr 18 15:28:35 EDT 2016
build-branch=master
build-date=Mon Apr 18 15\:28\:34 EDT 2016
build-hash=63d20fab1ba098bfe5a81bce8039099dc2693180
build-hash-short=63d20fa
build-msg=updated with al the test cases
build-number=77
build-release=1
build-type=RELEASE
build-version=16.1.77
build-year=16
module=Gradleware Test Client using..ARPECS Data API - test for gradleware git/release project
release-branch=master
release-date=Mon Apr 18 15\:28\:34 EDT 2016
release-hash=63d20fab1ba098bfe5a81bce8039099dc2693180
release-hash-short=63d20fa
release-msg=updated with al the test cases
release-number=1
release-type=RELEASE
release-version=16.1.77
release-year=16
submodule=Gradleware Test Client using..Authoritative Roles, Profile & Entitlements Control System (ARPECS)
```

