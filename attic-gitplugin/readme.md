#Changelog
## v1.2.1
* removed 'git push --tags' from Commit task to TagOnRelease task
* bumped version to 1.2.1 