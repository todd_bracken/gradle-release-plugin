# For Users of Plugin
## Required Dependencies (runtime)

* **git** - must be installed

* **git-plugin** gradle plugin<br>
::tasks:
  * gitShortHash
  * gitHash
  * gitCommitMsg

**Important Note:** *if you need the latest/greatest of the git-plugin and the release-plugin, please ask the Integration team to give you the URL to the central artifact server*

## To Use in your Build Script (Consumer)
---
**1 - Provide your project name and submodule/description**
```groovy
ext {
	projectName = 'My Project Name'
}
description = 'Description of my project'
```

**2 - Place the following at the end of your build script *(prior to your main target tasks like 'jar' or 'war')* **
```groovy
buildscript {
	repositories {
		maven	{ url "http://artifact-server:8080/artifactory/libs-cots" 	}
		jcenter()
	}
	dependencies {
		classpath group: 'bracken.gradle',	name: 'git-plugin',	version: '1.2'
		classpath group: 'bracken.gradle',	name: 'release-plugin',	version: '1.2'
	}
}
apply plugin: 'git-plugin'
apply plugin: 'release-plugin'

war.doFirst {
	tasks.handleVersioning.execute();
}

// War/Jar file configuration (the following is required, but there may be other information you require):
war {

	//Includes the version.properties and version.json files created by 'handle-versioning' task
	from (project.projectDir) {
		include 'version.*'
	}

	//... rest of the war configuration
}

// Place this above your uploadArchives to create a tag for release information
uploadArchives.doLast {
	tasks.gitTagOnRelease.execute();
}
```

**Note: do not set the pom.version in your uploadArchives - this will be handled by the release plugin **

## Configuration
---
### Automated Builds (CI/CD)
System/Environment Variables that MUST be set
```groovy
is-release=true/false
is-release-candidate=true/false
```
or
```groovy
go.is-release=true/false
go.is-release-candidate=true/false
```

### Manual Builds
To manually designate a release (when building via command line gradle).
Add the following to the command line when running your target task.

usage:
```bash
$gradle jar -Dis-release=true|false -Dis-release-candidate=true|false
```

## Usage / Tasks
---
```bash
$gradle tasks --all
```
### Task: handle-versioning
The handle-versioning task will put the following properties into the System properties and write them to **version.properties and version.json**
```groovy
'release-number'  		// '2'
'release-year'			// '15'
'build-number'			// '4'
'build-date'			// 'Wed Apr 22 16:44:04 EDT 2015'
'release-hash'			// '203ecfacaf0e2e8c372fb3196460399353679845'
'release-hash-short'	// '203ecfa'
'release-msg'			// 'this is my commit message for my project'
'release-date'			// 'Wed Apr 22 15:44:04 EDT 2015'
'release-version-label'	// '15.2.4-SNAPSHOT'
```
By way of including them at runtime you will also have access to these System.properties:
```groovy
'is-release'			//true|false
'is-release-candidate' 	//true|false
```

### Effect of environment variables on version label:
**e.g. starting with a fresh project in the year 2015 (baseline release version label would = 15.0.1**

| is-release | is-release-candidate |          archive result                  |        repository          |
|:----------:|:--------------------:|:---------------------------------------- |:--------------------------:|
|    true    |         true         | **15.1.1 (is-release takes precedence)** |      libs-gots-release     |
|    true    |         false        | **15.1.1**                               |      libs-gots-release     |
|    false   |         true         | **15.0.1-RC**                            |      libs-gots-snapshot    |
|    false   |         false        | **15.0.1-SNAPSHOT**                      |      libs-gots-snapshot    |



# For Developers of Plugin
## Building
---
```bash
release-plugin/$gradle clean uploadArchives --info
```
To customize the artifact repository that the plugin will be placed - change the 'repositories' properties of the mavenDeployer directive.
**By default the plugin will be generated at both the project level and in the local maven cache.**
```groovy
...
uploadArchives {
	repositories {
		mavenDeployer {
			repository(url: uri('../build/release'))
		}
		mavenLocal()
	}
}
...
```

## Testing
---
Use the gradlewares consumer-test sub-project to test and run different configurations of this plugin
e.g.
```bash
/gradlewares/consumer-test$gradle tasks
```
