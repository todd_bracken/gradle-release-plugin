# TO-DO:
* Add ability to enter Git Repo password on command line during execution
  * will require ability to set/provide remote during execution
* Possibly add a different tag for each of the release types in order to make the commands easier to understand without reading the README
* Add the current Git remote to the version.json manifest
* Add version info to the MANIFEST file

# Release Notes
* 17.1.2 - Added ability to set build number through -DbuildNum=<n> during execution
* 17.1.1 - Made Git 'push' optional through command line script
* 16.4.2 - fixed bug in disableFlashbackCheck
* 16.4.1 - added command line switch for 'disableFlashbackCheck' which will turn off Git --is-ancestor check for releases (see below for description)

# Gradle ReleasePlugin "Nutshell"
The ReleasePlugin is a utility meant to be imported into any Gradle build for the use in **Versioning** and tracking a project's **Releases** during Gradle build/archive events.  Internally it uses Git tagging and branching to track these types of releases:

* **Snapshots**
* **Release Candidates**
* **Releases** & **PATCH Releases**

It is meant to be used in tandem with a Continuous Delivery server like GoCD, but is not necessary for its operation.

**Note:** the ReleasePlugin takes an opinionated approach to build/release workflows in that it assumes that active development will always be on the main trunk ('master') of your git repository.  It also assumes (and automatically creates) Release Branches only.

## Problems Meant to Solve
##### 1. Tracing Released Code Back to the Build that Created It
* **From the running application..**
One of the outputs of the ReleasePlugin is a version.json & version.prop file.  These files are meant to be packaged along with and referenced by your application and its runtime to log or display its current build state.
* **To the built Archive sitting in your Artifact repository or server's filesystem...**
One of the most common questions for Ops folks to answer is the "what version of the application is deployed?".  Usually this is not always an easy question to answer, but with the archive naming (see below) convention along with the version.* files built into the archive - it should no longer be difficult to answer even if the archive file has been renamed.
* **To the code that produced it...**
Inspecting the version.* files, you can determine the tag, commit and branch that the build was based on. The ReleasePlugin uses Git tags and branches to track Release Candidates, Releases, and Patches.  Git tags mark the important events in the lifecycle of your application while Git Release branches can track Releases and the bug-fixes and patches that happen on those branches.
* **To the build pipeline that created it...**
When you know that a release originates from a specific tag in your Git repository and was built at a specific place in your CD system's pipeline chain - this knowledge makes empowers development teams to know that releases are being handled correctly and with repeatability.  

##### 2. Enabling Gradle Builds to Utilize a Standardized Versioning & Archive Scheme
###### Versioning Scheme is Simple...
e.g. **YY**+**Release**+**Build Number** -> 16.1.21

###### Archive Naming Builds on this...
e.g. **Version Number**+(optional)**-Release Type**

* 16.1.21-SNAPSHOT
* 16.1.21-RELCAND
* 16.1.21
* 16.1.21-PATCH

##### 3. Eliminating Unnecessary Builds & Ensuring that the Correct Builds are being Run in CD Server
The ReleasePlugin uses the built in Git 'Ancestor' check.  If a currently building project attempts to build/release code that is older (--is-ancestor) of another previously released baseline, the build will fail.

## Reference of Runtime Input/Output
![Plugin I/O](./plugin-io.png "Plugin I/O")

#### Version Number Reference e.g.
| First Position | Second Position | Third Position |
|:--------------:|:---------------:|:--------------:|
| **2 Digit Year** (current year) | **Release Number** (1..n) |  **Build Number** (1..n)|

* **e.g.** starting with a fresh project in the year 2016 (baseline release version label would = 16.1.1)
* **Note:** BUILD NUMBER is not really significant other than being a indication of uniqueness

## Getting Started - Configuring and Using ReleasePlugin
### Required Dependencies & Runtime Assumptions

* **git** installed and in path
  * **git remote** set with auth information set in remote url
  * **git local branch** = master
  * **git remote name** = origin
* **gradle** installed and in path

### To Use in your Build Script
**1. Provide your project name and submodule/description and artifactId (archive base name)**
```groovy
ext {
  	 artifactId = 'my-sso-jar' // what you want your base jar or war file name to be
	   projectName = 'My Project Name'
  	 archiveType = 'jar' // or war
}
description = 'Description of my project'
```

**2. Place the following at the end of your build script _(prior to your main target tasks like 'jar' or 'war')_**
```groovy
buildscript {
	repositories {
		jcenter()
	}
	dependencies {
		classpath group: 'bracken.gradle',	name: 'release-plugin',	version: '+'
	}
}
apply plugin: 'release-plugin'

jar.doFirst {
	//Optionally place here or run during gradle build/jar/uploadArchives (e.g. $gradle getNextVersionInfo build)
	tasks.getNextVersionInfo.execute();
}

// War/Jar file configuration (the following is required, but there may be other information you require):
jar {

	//Includes the version.properties and version.json files created by 'getNextVersionInfo' task
	from (project.projectDir) {
		include 'version.*'
	}

	//... rest of the war configuration
}
```
**Note: do not set the pom.version in your uploadArchives - this will be handled by the release plugin**

## Command Line Usage
### Gradle Tasks
```groovy

Release Versioning tasks
------------------------
getNextVersionInfo -
Increments version according to logic for SNAPSHOT / RELEASE CANDIDATE / RELEASE / PATCH
 Command Args:
  -DreleaseType=<release type> >> run: gradle printReleaseTypes for options
  -DdisableGit >> to bypass git tag/branch/push functions
 Note for RELEASE, RELEASE CANDIDATE and PATCH:
  Will have Git Tag/Branch/Push side-effects
  Assumes Git remote set as origin and has basic auth info in URL

printReleaseTypes -
Values for -DreleaseType=<option>

```
#### Command Line Arguments to $gradle build/jar/war/uploadArchives
```bash
$  -DreleaseType=<release type> \ # run $gradle printReleaseTypes for options
$  -DdisableGit \ # to bypass git tag/branch/push functions
$  -DdisableGitPush \ # to bypass only remote pushing of tags/branches [default: true] (note: will be disabled if 'disableGit'=true)
$  -DdisableFlashbackCheck # to turn off git merge-base --is-ancestor" check
$  -DbuildNum=21 # to add build number on command-line and not in system environment, will override System Environment if exists
```

### Automated Builds (CI/CD)
**If Environment property set (currently defaults to: 'GO_PIPELINE_COUNTER') - build number will follow the value of this property**

```bash
# build>
$gradle getNextVersionInfo jar -DreleaseType=s

# build / local maven install>
$gradle getNextVersionInfo install

# build and release>
$gradle getNextVersionInfo bintrayUpload -DapiKey=[apikey]
or..
$gradle uploadArchives -DartifactoryPwd=[artifactoryPwd]
```

#### Overriding the Default Environment (not System) Property...
*1. Add the following to your build file*
```groovy
tasks.getNextVersionInfo.with {  
	buildNumProvidedProp = 'MY_BUILDNO'
}
```
*2. Set the environment property for the build no(linux eg.)*
```bash
$export MY_BUILDNO=43
```

### Manual (Developer-initiated) Builds
```bash
# print release type info>
$gradle printReleaseTypes

# build>
$gradle jar -DreleaseType=r

# build / local maven install>
$gradle install

# build and release>
$gradle bintrayUpload -Dapikey=[apikey]
# or..
$gradle uploadArchives -DartifactoryPwd=[artifactoryPwd]

# Other Options
## ignore check for re-releasing same code baseline
```bash
$gradle getNextVersionInfo	jar -DreleaseType=r -DdisableGit
```

## disable all Git tasks (tagging,branching,commit of version.* files etc)
```bash
$gradle getNextVersionInfo jar -DreleaseType=r -DdisableGit
```

## enable all Git functionality EXCEPT remote pushing of tags/branches (WARNING: you must push manually after the build to properly keep versioning intact)
```bash
$gradle getNextVersionInfo jar -DreleaseType=r -DdisableGitPush
```

## Sample Output Version Files:
### version.json
```json
{
    "build-target-release": "1",
    "build-type": "RELEASE",
    "module": "Gradleware Test Client using..ARPECS Data API - test for gradleware git/release project",
    "release-build": "1",
    "build-version": "16.1.1",
    "build-msg": "added cleanReleaseBranches",
    "build-year": "16",
    "release-year": "16",
    "release-tag": "releases/16.1.1",
    "build-hash-short": "638307e",
    "release-version": "16.1.1",
    "plugin-version-used": "16.4",
    "release-msg": "added cleanReleaseBranches",
    "release-hash-short": "638307e",
    "build-date": "Wed May 11 13:41:41 EDT 2016",
    "release-branch": "release-branches/16.1.1",
    "release-number": "1",
    "release-date": "Wed May 11 13:41:41 EDT 2016",
    "build-number": "1",
    "build-branch": "release-branches/16.1.1",
    "build-hash": "638307eb08e3e2b9f779ae9505eb1b7d7d059a60",
    "build-tag": "releases/16.1.1",
    "release-hash": "638307eb08e3e2b9f779ae9505eb1b7d7d059a60",
    "submodule": "Gradleware Test Client using..Authoritative Roles, Profile & Entitlements Control System (ARPECS)"
}
```

### version.prop
```property

#Version Properties
#Wed May 11 13:41:43 EDT 2016
build-branch=release-branches/16.1.1
build-date=Wed May 11 13\:41\:41 EDT 2016
build-hash=638307eb08e3e2b9f779ae9505eb1b7d7d059a60
build-hash-short=638307e
build-msg=added cleanReleaseBranches
build-number=1
build-tag=releases/16.1.1
build-target-release=1
build-type=RELEASE
build-version=16.1.1
build-year=16
module=Gradleware Test Client using..ARPECS Data API - test for gradleware git/release project
plugin-version-used=16.4
release-branch=release-branches/16.1.1
release-build=1
release-date=Wed May 11 13\:41\:41 EDT 2016
release-hash=638307eb08e3e2b9f779ae9505eb1b7d7d059a60
release-hash-short=638307e
release-msg=added cleanReleaseBranches
release-number=1
release-tag=releases/16.1.1
release-version=16.1.1
release-year=16
submodule=Gradleware Test Client using..Authoritative Roles, Profile & Entitlements Control System (ARPECS)
```

#### [DEPRECATED: Release Plugin 16.3](v16.3-readme.md) v16.3-readme.md
#### [DEPRECATED: Release Plugin v1.1](v1.1-readme.md) v1.1-readme.md
#### [DEPRECATED: Release Plugin v1.2](v1.2-readme.md) v1.2-readme.md
#### [DEPRECATED: Release Plugin v1.2.1](v1.2.1-readme.md) v1.2.1-readme.md
