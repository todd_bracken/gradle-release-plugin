package bracken.gradle.release

import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

import bracken.gradle.release.tasks.VersioningTask;

class ReleaseTypeUtil {
		
	Logger logger
		
	public static enum ReleaseType {
		RELEASE				("RELEASE", 	(String[])["r", "release", "rel"]), 
		RELCAND				("RELCAND", 	(String[])["rc", "releaseCandidate", "relCand", "rel_cand"]), 
		SNAPSHOT			("SNAPSHOT", 	(String[])["s", "snapShot", "snap"])
		
		boolean isPatch = false
		String label
		String[] propValOptions 
		ReleaseType(String relTypeLabel, String[] vals) {
			label = relTypeLabel
			propValOptions = vals
		}
		
		public static ReleaseType getType(String comparison) {
			ReleaseType returnType = ReleaseType.SNAPSHOT // default
			for (ReleaseType rt : ReleaseType.values()) {
				rt.propValOptions.each {
					if(it.equalsIgnoreCase(comparison)) {
						returnType = rt
					}
				}
			}
			//println comparison
			//println returnType
			return returnType 
		}
		
		public String getTypeLabel() {
			def retLabel = ""
			if(this != ReleaseType.RELEASE)
				retLabel = "-" + label
			if(this.isPatch && this == ReleaseType.RELEASE) {
				retLabel = "-PATCH"
			}
			return retLabel
		}
			
		
		public static printOptions() {
			println String.format("%n[  %-12s  ][  %-35s  ]","RELEASE TYPE", "COMMAND LINE OPTION to -DreleaseType=<option>")
			def String options = "-------------------------------------------------------------------------\n"
			for (ReleaseType rt : ReleaseType.values()) {
				def label = rt.label
				if(rt == ReleaseType.SNAPSHOT) {
					label += " *default*"
				}
				options += String.format(" %-19s |  %s", "$label", "\$gradle <task> -DreleaseType=[ ")
				rt.propValOptions.each {
					 options += "$it | "
				}
				options = options.substring(0, options.length() -3)
				options += " ]\n"				
			}
			println options
		} 
		
	}	
	
	public ReleaseTypeUtil() {
		logger = Logging.getLogger(ReleaseTypeUtil.class)
	}
		
	def ReleaseType getReleaseType(String branch) {
		logger.info ('[RELEASE UTIL] Checking release type...')
		ReleaseType currRelType = ReleaseType.SNAPSHOT   // default
		
		// Check for -DreleaseType property to get current release type
		if(VersioningTask.cliArgs.releaseType != null) {
			logger.info("[RELEASE UTIL] Release Type (from command line '-DreleaseType=${VersioningTask.cliArgs.releaseType})")
			currRelType = ReleaseType.getType(VersioningTask.cliArgs.releaseType)
			logger.info('[RELEASE UTIL] Checking if this is a PATCH...')
			if(branch.startsWith(VersionConstants.git_ReleaseBranchPrefix)) {
				logger.lifecycle("[RELEASE UTIL] On Release Branch, Setting build as $currRelType/PATCH...")
				currRelType.isPatch = true
			}							
		}
						
		return currRelType
	}
}
