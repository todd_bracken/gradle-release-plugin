package bracken.gradle.release

import groovy.json.JsonBuilder

import java.io.File;

import bracken.gradle.release.GitUtil.ReleaseLog
import bracken.gradle.tasks.release.SortedProperties

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;


class ReleasePropertyMgr  {
	static Logger logger
	static {
		logger =  Logging.getLogger(ReleaseTypeUtil.class)
	}
		
	static String propFileName = 'version.prop'
	static String jsonFileName = 'version.json'
	static String oldPropFileName = 'version.properties'
	static File propsFile
	static SortedProperties buildProps
	static SortedProperties releaseProps
	
	static GitUtil gu = new GitUtil()

	def static ReleaseLog readyVersionProperties() {
		buildProps = new SortedProperties()
		releaseProps = new SortedProperties()
		
		logger.info("[RELEASE PROP MGR] Cleaining up prior plugin's properties file")
		def oldPropFile = new File(oldPropFileName)
		if(oldPropFile.exists()) {
			oldPropFile.delete()
		}
		logger.info("[RELEASE PROP MGR] Removing any prior version.* files")
		propsFile = new File(propFileName)
		if(propsFile.exists()){
			propsFile.delete()
		}
		def versJsonFile = new File(jsonFileName)
		if(versJsonFile.exists()) {
			versJsonFile.delete()
		}
				
		logger.info("[RELEASE PROP MGR] Getting Git info")
		setCurrGitTaskProps()
		
		def branch = getBuildProp(VersionConstants.buildProp_GitBranch)
		def lastReleaseLog = null
		if(!branch.startsWith(VersionConstants.git_ReleaseBranchPrefix)) {
			logger.info("[RELEASE PROP MGR] Checking for prior release history and getting last release number")
			lastReleaseLog = initReleaseInfo()
			if(lastReleaseLog != null)		
				logger.info("[RELEASE PROP MGR] Last version# ($lastReleaseLog.version)")
		}else {
			String t = "${VersionConstants.git_ReleaseTagPrefix}${branch.tokenize('/')[1]}"
			lastReleaseLog = gu.parseTagIntoLog(t)
			if(!lastReleaseLog.hash?.trim()) {
				throw new GradleException("[RELEASE PROP MGR] LAST RELEASE TAG MISSING ERROR, ABORTING... Attempting to build/release from a release branch, but corresponding release tag is missing!")
			}
			mapRelLogToProps(lastReleaseLog)
		}
		
		logger.info("[RELEASE PROP MGR] ReadyVersionProperties() completed...")
		
		return lastReleaseLog
	}
	
	def static setDefaultProjProps(String projName, String description) {		
		ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_Date, new Date().toString()) //only thing that ALWAYS changes
		ReleasePropertyMgr.setBuildProp(VersionConstants.projProp_Module, projName)
		ReleasePropertyMgr.setBuildProp(VersionConstants.projProp_SubModule, description)
		ReleasePropertyMgr.setBuildProp(VersionConstants.projProp_PluginVersionUsed, VersionConstants.projProp_PluginVersion)
	}
	
	def static ReleaseLog initReleaseInfo() {
		gu = new GitUtil()
		ReleaseLog relLog = gu.getThisYearsPreviousRelease()		
				
		if(relLog != null) {
			mapRelLogToProps(relLog)
		}		
		
		// To be backwards compatible with current UI elements...
		if(getBuildProp(VersionConstants.releaseProp_ReleaseVersion) == VersionConstants.notset) {
			setBuildProp(VersionConstants.releaseProp_ReleaseVersion, VersionConstants.notReleasedMsg)
		}
		if(getBuildProp(VersionConstants.releaseProp_Date) == VersionConstants.notset) {
			setBuildProp(VersionConstants.releaseProp_Date, VersionConstants.notReleasedMsg)
		}
		return relLog
	}
	
	def static mapRelLogToProps(ReleaseLog rl) {
		setBuildProp(VersionConstants.buildProp_ReleaseNumber, 		rl.release)
		setReleaseProp(VersionConstants.releaseProp_ReleaseBuild,	rl.build)
		setReleaseProp(VersionConstants.releaseProp_ReleaseNumber,	rl.release)
		setReleaseProp(VersionConstants.releaseProp_Year, 			rl.year)
		setReleaseProp(VersionConstants.releaseProp_Date, 			rl.dte.toString())
		setReleaseProp(VersionConstants.releaseProp_GitHash, 		rl.hash)
		setReleaseProp(VersionConstants.releaseProp_GitHashShort, 	rl.shortHash)
		setReleaseProp(VersionConstants.releaseProp_GitBranch, 		rl.branch)
		setReleaseProp(VersionConstants.releaseProp_ReleaseVersion,	"$rl.year.$rl.release.$rl.build")
		
		logger.debug("[RELEASE PROP MGR] Last Release Tag: ($rl.tag)")
		logger.debug("[RELEASE PROP MGR] Last Release Date: ($rl.dte)")
	}
	
	def static setCurrGitTaskProps() {
		gu = new GitUtil()
		setBuildProp(VersionConstants.buildProp_GitHash, gu.getGitHash())
		setBuildProp(VersionConstants.buildProp_GitHashShort, gu.getShortGitHash())
		setBuildProp(VersionConstants.buildProp_GitMsg, gu.getGitCommitMsg())
		setBuildProp(VersionConstants.buildProp_GitBranch, gu.getGitCurrBranch())
	}
	
	def static saveReleaseInfo() {
		
		//TODO: Switch to GitUtil version?...
		setReleaseProp(VersionConstants.releaseProp_ReleaseBuild,	getBuildProp(VersionConstants.buildProp_BuildNumber))
		setReleaseProp(VersionConstants.releaseProp_ReleaseNumber,	getBuildProp(VersionConstants.buildProp_ReleaseNumber))
		setReleaseProp(VersionConstants.releaseProp_Year, 			getBuildProp(VersionConstants.buildProp_Year))
		setReleaseProp(VersionConstants.releaseProp_Date, 			getBuildProp(VersionConstants.buildProp_Date))
		setReleaseProp(VersionConstants.releaseProp_GitHash, 		getBuildProp(VersionConstants.buildProp_GitHash))
		setReleaseProp(VersionConstants.releaseProp_GitHashShort, 	getBuildProp(VersionConstants.buildProp_GitHashShort))
		setReleaseProp(VersionConstants.releaseProp_GitMsg,			getBuildProp(VersionConstants.buildProp_GitMsg))
		setReleaseProp(VersionConstants.releaseProp_GitBranch, 		getBuildProp(VersionConstants.buildProp_GitBranch))
		setReleaseProp(VersionConstants.releaseProp_ReleaseVersion,	getBuildProp(VersionConstants.buildProp_BuildVersion))
	}		
	
	def static String getReleaseProp(String prop) {
		return getProp(prop, releaseProps)
	}
	
	def static String getBuildProp(String prop) {
		return getProp(prop, buildProps)
	}
	
	def private static String getProp(String prop, Properties props) {
		def val
		if (System.properties[prop] != null)
			val = System.getProperty(prop)
		else
			val = props.getProperty(prop)
		if(val == null)
			val = VersionConstants.notset  // default to zero
		return val
	}
	
	def static setReleaseProp(String prop, String val) {
		setProp(prop, val, releaseProps)
	}
		
	def static setBuildProp(String prop, String val) {
		setProp(prop, val, buildProps)
	}
	
	def private static setProp(String prop, String val, Properties props) {
		if(val == null) val = ''
		System.setProperty(prop, val) // sets in system
		props.setProperty(prop, val) // sets in version prop
		logger.info("[RELEASE PROP MGR] Setting Prop]> [version.properties.('$prop')]: $val")
	}
	
	def static savePropsToFiles(){
		// PROP File -- crazy amount of file handling necessary for some reason
		def FileOutputStream out = new FileOutputStream(propsFile)
		out.getChannel().force(false)
		SortedProperties allProps = mergeProps()
		try {
			allProps.store(out, 'Version Properties')
			out.flush()
			out.finalize()
		}finally {
			out.close()
		}
		// JSON File
		new File(jsonFileName).write(new JsonBuilder(allProps).toPrettyString())
		logger.info("[RELEASE PROP MGR]> Saved Version Properties and JSON files")
	}
	
	def private static SortedProperties mergeProps() {
		SortedProperties allProps = new SortedProperties()
		allProps.putAll(buildProps)
		if(releaseProps.size() != 0)
			allProps.putAll(releaseProps)
		return allProps
	}
		
	def static printReleaseInfo() {
		SortedProperties allProps = mergeProps()
		print getFormattedPropertiesWithHeader(allProps)
	}
		
	def static String getFormattedPropertiesWithHeader(Properties props) {
		def colW = VersionConstants.propOutputColWidth
		String vi = String.format("%n%-${colW}s  %s %n", 'Version Property', 'Value')
		vi += String.format("%-${colW}s  %s %n", '================', '=====')
		vi += getFormattedProperties(props)
		return vi
	}
	
	def static String getFormattedProperties() {
		getFormattedProperties(mergeProps())
	}
	
	def static String getFormattedProperties(Properties props) {
		def colW = VersionConstants.propOutputColWidth
		String vi = ""
		props.keys().each { key ->
			vi += String.format("%-${colW}.${colW}s  %s %n", key, props.getProperty(key))
		}
		return vi
	}

}
