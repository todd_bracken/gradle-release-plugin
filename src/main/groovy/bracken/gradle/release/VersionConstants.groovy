package bracken.gradle.release

import bracken.gradle.release.ReleaseTypeUtil.ReleaseType;

class VersionConstants {
	
	static final String projProp_Module = 'module'
	static final String projProp_SubModule = 'submodule'
	static final String projProp_PluginVersionUsed = 'plugin-version-used'
	static final String projProp_PluginVersion = '17.1.2'
	
	static final String buildProp_BuildNumber = 'build-number'
	static final String buildProp_ReleaseNumber = 'build-target-release'
	static final String buildProp_Year = 'build-year'	
	static final String buildProp_Date = 'build-date'
	static final String buildProp_GitHash = 'build-hash'
	static final String buildProp_GitHashShort = 'build-hash-short'
	static final String buildProp_GitMsg = 'build-msg'
	static final String buildProp_GitBranch = 'build-branch'
	static final String buildProp_BuildType = 'build-type'
	static final String buildProp_BuildVersion = 'build-version'
	static final String buildProp_BuildTag = 'build-tag'
	
	static final String releaseProp_ReleaseBuild =  'release-build'
	static final String releaseProp_ReleaseNumber =  'release-number'
	static final String releaseProp_Year = 'release-year'
	static final String releaseProp_Date = 'release-date'
	static final String releaseProp_GitHash = 'release-hash'
	static final String releaseProp_GitHashShort = 'release-hash-short'
	static final String releaseProp_GitMsg = 'release-msg'
	static final String releaseProp_GitBranch = 'release-branch'
	static final String releaseProp_ReleaseVersion = 'release-version'
	static final String releaseProp_ReleaseTag = 'release-tag'
	
	/* Git */
	static final String gitProp_Hash = 'githash'
	static final String gitProp_HashShort = 'githash_short'
	static final String gitProp_GitMsg = 'gitmsg'
	static final String gitProp_CurrBranch = 'gitbranch'
	static final String git_ReleaseBranchPrefix = 'release-branches/'
	static final String git_ReleaseTagPrefix = 'releases/'
	static final String git_ReleaseCandTagPrefix = 'releaseCandidates/'
	static final String git_PatchTagPrefix = 'patches/'
	static final String git_TagMsg = "tag created by automated Release Plugin v$VersionConstants.projProp_PluginVersion"
	static final String git_mainBranch = 'master'
	
	/* Command Line Switches & Options*/
	static final String cliSwitch_releaseType = 'releaseType'
	static final String cliSwitch_disableGit = 'disableGit' // don't allow any git actions as a result of a run
	static final String cliSwitch_disableGitPush = 'disableGitPush' // will allow tagging and branching, but won't try to push to remote
	static final String cliSwitch_disableFlashbackCheck = 'disableFlashbackCheck'
	static final String cliOption_BldNo= 'buildNum'
	
	/* Misc */	
	static final String notset = '0'
	static final String notReleasedMsg = 'no current release'
	static final String propOutputColWidth = '25'
	static final String releaseBranchPrefix = 'release-branches/'	
}
