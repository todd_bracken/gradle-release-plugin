package bracken.gradle.release

import java.util.Date;

import org.gradle.api.GradleException
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

import bracken.gradle.GroovyTestRunner.GitLog;
import bracken.gradle.release.ReleaseTypeUtil.ReleaseType
import bracken.gradle.tasks.release.SortedProperties;

class GitUtil {
	
	Logger logger
	
	@groovy.transform.Sortable(excludes = ['msgProps'])
	@groovy.transform.ToString
	class ReleaseLog {
		// parsed version of Git log date
		Date dte
		// raw date from Git Log
		String tagDate
		String tag
		String hash
		String shortHash
		String branch
		String year
		String release
		String build
		String version
		SortedProperties msgProps
	}
	
	public GitUtil() {
		logger = Logging.getLogger(GitUtil.class)
	}
	
	/*
	 * GETTERS
	 */
	def Date getDateFromGitHash(String hash) {
		def gitCmd = ['git', 'show', '-s', '--format=%ci', hash].execute()
		return GitUtil.parseDateFromGitLogDate(gitCmd.getText().trim())
	}
	 
	def String getGitHash() {
		return getGitHash('HEAD')
	}
	
	def String getGitHash(String commitOrTag) {
		def githash = ['git', 'rev-list', '-n', '1', commitOrTag].execute().getText().trim()
		logger.info( '[RELEASE (GIT)] Long Hash [System.property(\''+ VersionConstants.gitProp_Hash + '\')]: ' + githash)
		System.setProperty(VersionConstants.gitProp_Hash, githash)
		return githash
	}
		
	def String getShortGitHash() {
		return getShortGitHash('HEAD')
	}
	
	def String getShortGitHash(String commit) {
		def githash_short = ['git', 'rev-parse', '--short', commit].execute().getText().trim()
		logger.info('[RELEASE (GIT)] Short Hash [System.property(\''+ VersionConstants.gitProp_HashShort + '\')]: ' + githash_short)
		System.setProperty(VersionConstants.gitProp_HashShort, githash_short)
		return githash_short
	}
	
	def String getGitCommitMsg() {
		return getGitCommitMsg('HEAD')
	}
	
	def String getGitCommitMsg(String commit) {
		def gitmsg = ['git', 'log', '-1', '--pretty=%B', commit].execute().getText().trim()
		logger.info('[RELEASE (GIT)] Commit Msg [System.property(\''+ VersionConstants.gitProp_GitMsg + '\')]: ' + gitmsg)
		System.setProperty(VersionConstants.gitProp_GitMsg, gitmsg)
		return gitmsg
	}
	
	def String getGitCurrBranch() {
		def gitbranch = ['git', 'rev-parse', '--abbrev-ref', 'HEAD'].execute().getText().trim()
		logger.info( '[RELEASE (GIT)] Current Branch [System.property(\''+ VersionConstants.gitProp_CurrBranch + '\')]: ' + gitbranch)
		System.setProperty(VersionConstants.gitProp_CurrBranch, gitbranch)
		return gitbranch
	}
	
	def ReleaseLog getThisYearsPreviousRelease() {
		def date = new Date()
		def year2digit = date.format('yy')
		def prefix = "$VersionConstants.git_ReleaseTagPrefix$year2digit"
		return getLastReleaseByTagPrefix(prefix)		
	}
	
	def ReleaseLog getPatchInfoByReleaseVersion(String version) {
		return getLastReleaseByTagPrefix(VersionConstants.git_ReleaseTagPrefix+version)
	}
		
	def static Date parseDateFromGitLogDate(String glDate) {
		//2016-04-18 15:32:23 -0400
		Date d = Date.parse("yyyy-mm-dd HH:mm:ss z", glDate)
		return d
	}	
	
	def private ReleaseLog getLastReleaseByTagPrefix(String tagPrefix) {
		logger.info("[RELEASE (GIT)] Fetching tags from Remote (origin)")
		def fetchCmd = ['git', 'fetch', 'origin', '--tags'].execute()
		fetchCmd.waitFor()		
		
		tagPrefix += "*"
		logger.info("[RELEASE (GIT)] Trying to match any tags in tag group: ($tagPrefix)")
		def tagCmd = ['git', 'tag', '-l', tagPrefix]
		def tagOutputStream = new StringBuffer()
		tagCmd.execute().waitForProcessOutput(tagOutputStream, System.err)
		def tagcount = tagOutputStream.readLines().size()
		if(tagcount == 0) {
			logger.info("[RELEASE (GIT)] No Tags matching ($tagPrefix)")
			return null
		}
			
		def logOutputArray = []
		tagOutputStream.eachLine {currtag, count ->
			def formatCmd =  ['git', 'log', '--format=format:%ai', '-1', currtag]
			def logOut = new StringBuffer()
			formatCmd.execute().waitForProcessOutput(logOut, System.err)
			logOut.eachLine {
				logOutputArray << new ReleaseLog(dte: parseDateFromGitLogDate(it), tagDate: it, tag: currtag)
				logger.debug("$it $currtag")
			}
		}
		//println logOutputArray.reverse()
		ReleaseLog l = logOutputArray.sort { a, b-> b.dte <=>a.dte}[0]
		logger.debug("[RELEASE (GIT)] Found Release using Tag ($l.tag)")
		
		// Fill in the rest of the data fields for ReleaseLog
		if(l != null) {
			l = parseTagIntoLog(l)
			//l = getTagMessage(l)
		}		
		return l		
	}
		
	def ReleaseLog parseTagIntoLog(ReleaseLog log) {
		if(log == null) return null
		logger.debug("[RELEASE (GIT)] Used tag for version: $log.tag ($log.tagDate)")
		def releaseParts = log.tag.tokenize('.')
		log.year = releaseParts[0].tokenize('/')[1]
		log.release = releaseParts[1]
		log.build = releaseParts[2]
		log.hash = getGitHash(log.tag)
		log.shortHash = getShortGitHash(log.hash)
		log.version = log.tag.tokenize('/')[1]
		log.branch = VersionConstants.git_ReleaseBranchPrefix + log.version
		return log
	}
	
	def ReleaseLog parseTagIntoLog(String tag) {
		ReleaseLog log = new ReleaseLog(tag: tag)
		def releaseParts = log.tag.tokenize('.')
		log.year = releaseParts[0].tokenize('/')[1]
		log.release = releaseParts[1]
		log.build = releaseParts[2]
		log.hash = getGitHash(log.tag)
		log.shortHash = getShortGitHash(log.hash)
		log.dte = getDateFromGitHash(log.hash)
		log.version = log.tag.tokenize('/')[1]
		log.branch = VersionConstants.git_ReleaseBranchPrefix + log.version
		return log
	}
			
	def boolean isFlashbackBuild(CommandArgs cliArgs) {
		//child: 850b321e7d4f8e3e231eb24ecd07185a815852c3
		//parent: 0a77bf6962755309d218b983a30792ce6b6a3c76
		//git merge-base --is-ancestor 0a77bf6962755309d218b983a30792ce6b6a3c76 850b321e7d4f8e3e231eb24ecd07185a815852c3 #should be true
		//git merge-base --is-ancestor 850b321e7d4f8e3e231eb24ecd07185a815852c3 0a77bf6962755309d218b983a30792ce6b6a3c76 #should be false
		def isFlashback = false
		
		if(!cliArgs.gitDisabled && !cliArgs.flashbackCheckDisabled) {
			def prevReleaseHash = ReleasePropertyMgr.getReleaseProp(VersionConstants.releaseProp_GitHash)
			def currReleaseHash = ReleasePropertyMgr.getReleaseProp(VersionConstants.buildProp_GitHash)
			logger.info("[RELEASE (GIT)] Previous Release: $prevReleaseHash")
			logger.info("[RELEASE (GIT)] Current Release: $currReleaseHash")
			
			if(prevReleaseHash != VersionConstants.notset) {
				def gitCmd = ['git', 'merge-base', '--is-ancestor', prevReleaseHash, currReleaseHash].execute()
				gitCmd.waitFor()
				if(gitCmd.exitValue() > 0)
					isFlashback = true
			}
		}
		return isFlashback
	}
	
	/*
	 * ACTIONS
	 */		
	def createTag(CommandArgs cliArgs, String tag, String commitHash, String msg) {
		if(!cliArgs.gitDisabled) {
						logger.lifecycle("[RELEASE (GIT)] Creating Git Tag ($tag) on commit ($commitHash)...")
			def gitCmd = ['git', 'tag', '-a', '-f', tag, commitHash, '-m', msg].execute()
			gitCmd.waitFor()
			if(gitCmd.exitValue() > 0)
				throw new GradleException("[RELEASE] FATAL Git Create Tag Failed!")
		}
	}
	
	def pushTag(CommandArgs cliArgs, String tag) {
		if(!cliArgs.gitDisabled && !cliArgs.gitPushDisabled) {
			logger.lifecycle("[RELEASE (GIT)] Pushing Git Tag ($tag) to 'origin'...")
			def gitCmd = ['git','push','origin', tag, '-f'].execute()
			gitCmd.waitFor()
			if(gitCmd.exitValue() > 0)
				throw new GradleException("[RELEASE] FATAL Git Push Tag to Remote Failed!")
		}
	}
	
	def createReleaseBranch(CommandArgs cliArgs, String branch, String commitHash) {
		if(!cliArgs.gitDisabled) {
			logger.lifecycle("[RELEASE (GIT)] Creating Release Branch ($branch) on commit ($commitHash)...")
			def gitCmd = ['git', 'branch', branch, commitHash].execute()
			gitCmd.waitFor()
			if(gitCmd.exitValue() > 0)
				throw new GradleException("[RELEASE] FATAL Git Create Release Branch Failed!")
		}
	}
	
	def pushBranch(CommandArgs cliArgs, String branch) {
		if(!cliArgs.gitDisabled && !cliArgs.gitPushDisabled) {
			logger.lifecycle("[RELEASE (GIT)] Pushing Release Branch ($branch) to 'origin'...")
			def gitCmd = ['git', 'push', 'origin', branch].execute()
			gitCmd.waitFor()
			if(gitCmd.exitValue() > 0)
				throw new GradleException("[RELEASE] FATAL Git Push Release Branch to Remote Failed!")
		}
	}
}
