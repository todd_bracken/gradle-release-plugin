package bracken.gradle.release.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskAction

import bracken.gradle.release.CommandArgs
import bracken.gradle.release.GitUtil
import bracken.gradle.release.ReleaseTypeUtil;
import bracken.gradle.release.VersionConstants;
import bracken.gradle.release.ReleasePropertyMgr;
import bracken.gradle.release.GitUtil.ReleaseLog
import bracken.gradle.release.ReleaseTypeUtil.ReleaseType
import groovy.json.JsonBuilder

class VersioningTask extends DefaultTask {

	/* RUNTIME PROVIDED BUILDNUM PROPERTY (DEFAULT PROP NAME)*/
	/* default is GO_PIPELINE_COUNTER, if overwritten, look for that property */
	String buildNumProvidedProp = 'GO_PIPELINE_COUNTER'

	/*  If this is overwritten (in build file) to false - Release system will create an incremented build number */
	boolean buildNumProvided = true

	ReleaseType releaseType = ReleaseType.SNAPSHOT 	// Default

	ReleaseLog lastReleaseLog

	def String projName = ''
	GitUtil gu

	static CommandArgs cliArgs

	VersioningTask() {
		logger.debug('[RELEASE] -SYSTEM- PROPERTIES (orig): ')
		logger.debug(ReleasePropertyMgr.getFormattedPropertiesWithHeader(System.getProperties()))

		// Set Command Line Arguments
		cliArgs = new CommandArgs()
		logger.info(cliArgs.printArgs())

		// Initialize the GitUtil
		gu = new GitUtil()
	}

	@TaskAction
	def setBuildAndReleaseInfo() {
		// Initialize ReleaseProperties & get Last Release Number
		lastReleaseLog = ReleasePropertyMgr.readyVersionProperties()

		// Set Project-level Properties
		if(project.hasProperty('projectName')) {
			projName = project.projectName
		}
		ReleasePropertyMgr.setDefaultProjProps(projName, project.description)

		logger.lifecycle("[RELEASE] Gradle Release Plugin version $VersionConstants.projProp_PluginVersion")
		logger.lifecycle("[RELEASE] Preparing Release Version Info for Project: $projName")

		setCurrentReleaseType()

		setCurrentReleaseYear()
		setCurrentReleaseNumber()
		setCurrentBuildNumber()
		setCurrentReleaseTypeLabel()

		project.version = ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_BuildVersion)

		//TODO: check if this applies to Patches
		logger.lifecycle("[RELEASE] Checking for 'Flashback' release (i.e. has a newer version already been released)")
		if(gu.isFlashbackBuild(cliArgs)) {
			throw new GradleException("[RELEASE] 'FLASHBACK' ERROR, ABORTING... ATTEMPTING TO RELEASE OLDER VERSION OF CODEBASE AFTER A NEWER COMMIT HAS ALREADY BE RELEASED")
		}

		switch(releaseType) {
			case ReleaseType.RELEASE:
				def tag = (releaseType.isPatch ? VersionConstants.git_PatchTagPrefix : VersionConstants.git_ReleaseTagPrefix)+project.version
				gu.createTag(cliArgs,
						tag,
						ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_GitHash),
						VersionConstants.git_TagMsg)
				gu.pushTag(cliArgs, tag)
				ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_BuildTag, tag)
				ReleasePropertyMgr.setReleaseProp(VersionConstants.releaseProp_ReleaseTag, tag)

				if(!releaseType.isPatch) {
					def branch = VersionConstants.git_ReleaseBranchPrefix+project.version
					gu.createReleaseBranch(cliArgs,
							branch,
							ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_GitHash))
					gu.pushBranch(cliArgs, branch)
					ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_GitBranch, branch)
					ReleasePropertyMgr.setReleaseProp(VersionConstants.releaseProp_GitBranch, branch)
				}

				ReleasePropertyMgr.saveReleaseInfo()
				break;

			case ReleaseType.RELCAND:
				def tag = VersionConstants.git_ReleaseCandTagPrefix+project.version
				gu.createTag(cliArgs,
						tag,
						ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_GitHash),
						VersionConstants.git_TagMsg)
				gu.pushTag(cliArgs, tag)
				ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_BuildTag, tag)
				break;

			default:
				break;
		}


		ReleasePropertyMgr.savePropsToFiles()

		logger.lifecycle('[RELEASE] LAST RELEASE: ' + ReleasePropertyMgr.getBuildProp(VersionConstants.releaseProp_ReleaseVersion))
		logger.lifecycle('[RELEASE] CURRENT BUILD:   ' + ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_BuildVersion))
		logger.info('[RELEASE] -VERSION- PROPERTIES: ')
		logger.info(ReleasePropertyMgr.getFormattedProperties())
	}

	/* Build Archive Suffix based on ReleaseType*/

	def setCurrentReleaseType() {
		ReleaseTypeUtil relUtil = new ReleaseTypeUtil()
		releaseType = relUtil.getReleaseType(ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_GitBranch))
		ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_BuildType, releaseType.toString())
		def patch = releaseType.isPatch ? "(PATCH)" : ""
		logger.lifecycle("[RELEASE] Release Type: $releaseType $patch")
	}

	def String setCurrentReleaseTypeLabel() {
		// -SNAPSHOT , -RELCAND, -PATCH, etc
		def label = ReleasePropertyMgr.buildProps.getProperty(VersionConstants.buildProp_Year) + '.' +
				ReleasePropertyMgr.buildProps.getProperty(VersionConstants.buildProp_ReleaseNumber) + '.' +
				ReleasePropertyMgr.buildProps.getProperty(VersionConstants.buildProp_BuildNumber) +
				releaseType.getTypeLabel()

		ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_BuildVersion, label)
		return label
	}

	/* Year Number Management */

	def setCurrentReleaseYear() {
		if(releaseType.isPatch) {
			ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_Year, lastReleaseLog.year)
		}else {
			def date = new Date()
			def formattedDate = date.format('yy')
			ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_Year, formattedDate)
		}
	}

	/* Release Number Management */

	def String setCurrentReleaseNumber() {
		def relnum = ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_ReleaseNumber)

		//Logic Change: release number will now always be currRelease + 1 (except for PATCH where it will remain the same)
		if(!releaseType.isPatch) {
			relnum = increment(relnum)
		}

		ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_ReleaseNumber, relnum)

		return relnum
	}

	/* Build Number Management */

	def String setCurrentBuildNumber() {
		def buildnum =  VersionConstants.notset

		// Logic: Build Number changes for every build
		if(!buildNumProvided) {
			buildnum = getBuildNumAutoIncremented()
		}else {
			buildnum = getBuildNumProvided()
		}

		if(buildnum == null || buildnum == VersionConstants.notset)
			buildnum = '1' // its always going to be least 1

		ReleasePropertyMgr.setBuildProp(VersionConstants.buildProp_BuildNumber, buildnum)

		return buildnum
	}

	/** Provided default = true (i.e. build number coming in through external property - like CD server)	 */
	def String getBuildNumProvided() {
		def buildnum = VersionConstants.notset
		if (System.getenv(buildNumProvidedProp) != null) {
			buildnum = System.getenv(buildNumProvidedProp)
			logger.info("[RELEASE] Build# Has been provided by environment variable ($buildNumProvidedProp):  $buildnum")
		}else if (cliArgs.buildNumber != 'NA') {
			buildnum = cliArgs.buildNumber
		}else {
			logger.info("[RELEASE] Warning! Build# set to provided - but property ($buildNumProvidedProp) not set!  Will use default auto-incrementing")
			buildnum = getBuildNumAutoIncremented()  // default to auto incrementing
		}

		return buildnum.toString()
	}

	def String getBuildNumAutoIncremented() {
		def buildnum = ReleasePropertyMgr.getBuildProp(VersionConstants.buildProp_BuildNumber)

		if(buildnum == null) {
			buildnum = 0
		}

		buildnum = increment(buildnum)

		return buildnum.toString()
	}


	/* Misc */

	def String increment(String relval) {
		def int val = relval.toInteger()
		val++
		return val
	}
}
