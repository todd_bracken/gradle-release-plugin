package bracken.gradle.release.tasks.attic

import org.gradle.api.Plugin
import org.gradle.api.Project

import bracken.gradle.git.tasks.*;

class GitPlugin implements Plugin<Project> {
    void apply(Project project) {
		project.task('gitHash', group: 'gitPlugin', type: GitHashTask, description: 'Gets the git full hash for last commit and puts into System.properties as: githash')
		project.task('gitShortHash', group: 'gitPlugin', type: GitShortHashTask, description: 'Gets the git short hash for last commit and puts into System.properties as: githash_short')
		project.task('gitCommitMsg', group: 'gitPlugin', type: GitCommitMsgTask, description: 'Gets the git commit message for last commit and puts into System.properties as: gitmsg')
		project.task('gitTagOnReleaseAndPush', group: 'gitPlugin',  type: GitTagOnReleaseTask, 
						description: 'Tags the current local repository with Release/Release Candidate Info and Pushes tag to default remote)')
		project.task('gitCommitAndPush', group: 'gitPlugin', type: GitCommitTask, 
						description: '(1) Performs a git add (defaults to just the version.* files, but takes file string in config stage), (2) git commit (commitMsg can be configged or passed via -DcommitMsg=<msg>), (3) git push to default remote')
		project.task('gitCurrentBranch', group: 'gitPlugin', type: GitCurrentBranchTask, description: 'Gets the current branch that is currently active and puts into System.properties: gitbranch')
		
	}
}
