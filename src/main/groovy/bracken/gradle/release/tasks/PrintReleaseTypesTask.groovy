package bracken.gradle.release.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskAction

import bracken.gradle.release.ReleaseTypeUtil.ReleaseType
import groovy.json.JsonBuilder

class PrintReleaseTypesTask extends DefaultTask {

	@TaskAction
	def printReleaseTypes() {
		ReleaseType.printOptions()
	}
}
