package bracken.gradle.release

class CommandArgs {
	String releaseType
	boolean gitDisabled
	boolean gitPushDisabled
	boolean flashbackCheckDisabled
	String	buildNumber

	CommandArgs() {
		releaseType = System.properties[VersionConstants.cliSwitch_releaseType]
		gitDisabled = System.properties[VersionConstants.cliSwitch_disableGit]==null ? false : true
		buildNumber = System.properties[VersionConstants.cliOption_BldNo]==null ? 'NA' : System.properties[VersionConstants.cliOption_BldNo]
		def dGP = System.properties[VersionConstants.cliSwitch_disableGitPush]
		gitPushDisabled = (dGP==null) 
			? true
			: (dGP.toLowerCase() == 'true' || dGP.toLowerCase() == 'false') 
				? dGP.toBoolean()
				: true 
		flashbackCheckDisabled = System.properties[VersionConstants.cliSwitch_disableFlashbackCheck]==null ? false : true
	}

	def String printArgs() {
		def colW = VersionConstants.propOutputColWidth
		String args = String.format("%n%-${colW}s  %-10s %s %n", 'Command Line Argument', 'Value', 'Description')
		args += String.format("%-${colW}s  %-10s %s %n", '======================', '=====', '=======================================')
		args += String.format("%-${colW}.${colW}s  %-10s %s %n", VersionConstants.cliSwitch_releaseType, releaseType, '[default: snapshot]')
		args +=  String.format("%-${colW}.${colW}s  %-10s %s %n", VersionConstants.cliSwitch_disableGit, gitDisabled, 'Disables all Git activity (tag/branch/push, etc) [default: false]')
		args +=  String.format("%-${colW}.${colW}s  %-10s %s %n", VersionConstants.cliSwitch_disableGitPush, gitPushDisabled, 'Disables Git pushes to remote repo [default: true]')
		args +=  String.format("%-${colW}.${colW}s  %-10s %s %n", VersionConstants.cliSwitch_disableFlashbackCheck, flashbackCheckDisabled, 'To turn off git merge-base --is-ancestor check [default: false]')
		args +=  String.format("%-${colW}.${colW}s  %-10s %s %n", VersionConstants.cliOption_BldNo, buildNumber, 'If set, will be used, otherwise will default to system env property (see readme)')
		return args
	}
	
	static void main(String[] args) {
		CommandArgs ca = new CommandArgs();
		print ca.printArgs();
	}
}
