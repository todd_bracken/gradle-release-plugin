package bracken.gradle

import org.gradle.api.Project
import org.gradle.api.Plugin

import bracken.gradle.release.tasks.*;

class ReleasePlugin implements Plugin<Project> {
	void apply(Project project) {
		project.task('printReleaseTypes', group: 'Release Versioning', type: PrintReleaseTypesTask)
		project.tasks.'printReleaseTypes'.description = 'Values for -DreleaseType=<option>\n'
				
		project.task('getNextVersionInfo', group: 'Release Versioning',  type: VersioningTask)
		project.tasks.'getNextVersionInfo'.description = '\nIncrements version according to logic for SNAPSHOT / RELEASE CANDIDATE / RELEASE / PATCH \n Command Args: \n  -DreleaseType=<release type> >> run: gradle printReleaseTypes for options \n  -DdisableGit >> to bypass git tag/branch/push functions\n Note for RELEASE, RELEASE CANDIDATE and PATCH:\n  Will have Git Tag/Branch/Push side-effects\n  Assumes Git remote set as origin and has basic auth info in URL\n'
	}
}
