package bracken.gradle

import bracken.gradle.release.CommandArgs;
import bracken.gradle.release.VersionConstants;
import bracken.gradle.release.ReleasePropertyMgr;
import bracken.gradle.tasks.release.SortedProperties;

class GroovyTestRunner {
		
	public static void main(String[] args) {
		GroovyTestRunner tr = new GroovyTestRunner()
		//testPropFormatter();
		testCLI();
	}
	
	def static testCLI() {
		System.properties[VersionConstants.cliSwitch_disableGitPush]='FALSE'
		CommandArgs cliArgs = new CommandArgs()
		println cliArgs.gitPushDisabled
		System.properties[VersionConstants.cliSwitch_disableGitPush]='false'
		cliArgs = new CommandArgs()
		println cliArgs.gitPushDisabled
		System.properties[VersionConstants.cliSwitch_disableGitPush]='jibberish'
		cliArgs = new CommandArgs()
		println cliArgs.gitPushDisabled
		cliArgs = new CommandArgs()
		println cliArgs.gitPushDisabled
		System.properties[VersionConstants.cliSwitch_disableGitPush]='true'
		cliArgs = new CommandArgs()
		println cliArgs.gitPushDisabled
		
		print cliArgs.printArgs();
	}
	
	def static testPropFormatter() {
		SortedProperties props = new SortedProperties()
		props.put("this", "that")
		props.put("buckle", "shoe")
		props.put("one", "two")
		props.put("plugin-version-used", "16.4.1")
		print ReleasePropertyMgr.getFormattedProperties(props)
		print ReleasePropertyMgr.getFormattedPropertiesWithHeader(props)
	}
	
	def testGitAncestry() {
		def prevReleaseHash = '0a77bf6962755309d218b983a30792ce6b6a3c76'
		def currReleaseHash = '850b321e7d4f8e3e231eb24ecd07185a815852c3'
		if(prevReleaseHash == 'something other') {
			System.exit(0)
		} else {
			def gitCmd = "git merge-base --is-ancestor $prevReleaseHash $currReleaseHash ".execute()
			gitCmd.waitFor()
			assert 1 == gitCmd.exitValue()
			//println gitCmd.exitValue() //System.exit("git merge-base --is-ancestor $currReleaseHash $prevReleaseHash".execute().exitValue())
		}
		
		//child: 850b321e7d4f8e3e231eb24ecd07185a815852c3
		//parent: 0a77bf6962755309d218b983a30792ce6b6a3c76
		//git merge-base --is-ancestor 0a77bf6962755309d218b983a30792ce6b6a3c76 850b321e7d4f8e3e231eb24ecd07185a815852c3; echo $?  #should be true
		//git merge-base --is-ancestor 850b321e7d4f8e3e231eb24ecd07185a815852c3 0a77bf6962755309d218b983a30792ce6b6a3c76; echo $? #should be false
	}
	
	@groovy.transform.Sortable
	@groovy.transform.ToString
	class GitLog {
		Date dte
		String tagDate
		String tag
	}
	
	
	/**
	 * @param tagGroup (e.g. 'releases')
	 * @return null or GitLog object
	 */
	def GitLog getCurrYearsLastReleaseGitLogFromTagGroup(String tagGroup) {
		def date = new Date()
		def year2digit = date.format('yy')
		
		def tagCmd = "git tag -l $tagGroup/$year2digit*"
		def tagOutputStream = new StringBuffer()
		tagCmd.execute().waitForProcessOutput(tagOutputStream, System.err)
		def tagcount = tagOutputStream.readLines().size()
		if(tagcount == 0)
			return null
			
		def logOutputArray = []
		tagOutputStream.eachLine {currtag, count ->
			def formatCmd =  "git log --format=format:%ai -1 $currtag"
			def logOut = new StringBuffer()
			formatCmd.execute().waitForProcessOutput(logOut, System.err)
			logOut.eachLine {
				//2016-04-18 15:32:23 -0400
				Date d = Date.parse("yyyy-mm-dd HH:mm:ss z", it)
				logOutputArray << new GitLog(dte: d, tagDate: it, tag: currtag)
				println "$it $currtag"
			}			
		}
		//println logOutputArray.reverse()
		return logOutputArray.sort { a, b-> b.dte <=>a.dte}[0]
		
	}
	
	def int getCurrReleaseNumberFromTag(GitLog log) {
		if(log == null) return 0		
		def tag = log.tag
		def releasenumbers = tag.tokenize('.')
		println "Used tag for version: $gl.tag ($gl.tagDate)"
		return releasenumbers[1].toInteger()
	}
	
	def String[] getTagMessage(GitLog log) {
		if (log == null) return []
		def tag = log.tag
		def showCmd = "git show $tag"
		def showOut= new StringBuffer()
		showCmd.execute().waitForProcessOutput(showOut, System.err)
				
		def collectMsgDelimCount = 0
		def msg = [:]
		showOut.eachLine {logLine ->
			if(logLine.length() == 0 && collectMsgDelimCount == 0) {
				collectMsgDelimCount++
				//println "STARTING COLLECTION!"
			} else if(logLine.length() == 0 && collectMsgDelimCount == 1) {
				collectMsgDelimCount++
				//println "FINISHING COLLECTION!"
			}
			if(collectMsgDelimCount == 1 && logLine.length() > 0) {
				//println "This is part of the message: $logLine"
				if(logLine.contains("=")) {
					def lineParts = logLine.tokenize("=")
					msg.put(lineParts[0], lineParts[1])
				}
			}
		}
		return msg		
	}
	

/* -- Git Tag with Message
tag releases/16.2.1
Tagger: Todd Bracken <todd.bracken@nesassociates.com>
Date:   Wed Apr 27 09:54:20 2016 -0400

build-branch=master
build-date=Tue Apr 19 15\:48\:36 EDT 2016
build-hash=d8e2dbfccace98b63d2dfad2412cb214a43ece31
build-hash-short=d8e2dbf
build-msg=[gradleware.releaseplugin]\:  Automated version bump from Release Versioning system
build-number=79
build-release=1
build-type=SNAPSHOT
build-version=16.1.79-SNAPSHOT
build-year=16
module=Gradleware Test Client using..ARPECS Data API - test for gradleware git/release project
release-branch=master
release-date=Mon Apr 18 15\:28\:34 EDT 2016
release-hash=63d20fab1ba098bfe5a81bce8039099dc2693180
release-hash-short=63d20fa
release-msg=updated with al the test cases
release-number=1
release-type=RELEASE
release-version=16.1.77
release-year=16
submodule=Gradleware Test Client using..Authoritative Roles, Profile & Entitlements Control System (ARPECS)

commit b0a0657680fbcff6519a1d747af650f8ec32754d
Author: Todd Bracken <todd.bracken@nesassociates.com>
Date:   Mon Apr 25 15:41:15 2016 -0400

    adding submodule

diff --git a/.gitmodules b/.gitmodules
new file mode 100644
index 0000000..eb62836
--- /dev/null
+++ b/.gitmodules

*/
	
/* -- Git Tag with no Message (-m "")
tag patches/16.1.2-PATCH
Tagger: Todd Bracken <todd.bracken@nesassociates.com>
Date:   Wed Apr 27 11:49:16 2016 -0400


commit f34c7652a42ab2931500aeb8c076467e5558a14c
Author: Todd Bracken <todd.bracken@nesassociates.com>
Date:   Tue Apr 26 10:28:24 2016 -0400

    this is another branch commit

diff --git a/someotherbranchfile.txt b/someotherbranchfile.txt
new file mode 100644
index 0000000..4c5b667
--- /dev/null
+++ b/someotherbranchfile.txt
@@ -0,0 +1 @@
+this is another branch file
*/
		
}
